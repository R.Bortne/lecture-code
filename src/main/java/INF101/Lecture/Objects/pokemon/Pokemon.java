package INF101.Lecture.Objects.pokemon;

public class Pokemon {
    
    String name;
    int level;

    public Pokemon(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public void speak() {
        System.out.println(name + " " +  name);
    }

    public void levelUp() {
        level++;
    }

}
