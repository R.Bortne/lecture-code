package INF101.Lecture.Objects;
import java.util.ArrayList;

public class Persons {

	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Integer> ages = new ArrayList<Integer>(); 
		
		fill(names, ages);
		printAll(names, ages);
		removeLast(names, ages);
		System.out.println();
		printAll(names, ages);
	}

	private static void removeLast(ArrayList<String> names, ArrayList<Integer> ages) {
		names.remove(names.size()-1);
		ages.remove(names.size()-1);
	}

	/**
	 * Fills two lists with the same number of elements.
	 * names.get(i) and ages.get(i) are the name and age of person i
	 *  
	 * @param names The List of names to be filled
	 * @param ages The List of ages to be filled
	 */
	public static void fill(ArrayList<String> names, ArrayList<Integer> ages) {
		names.add("Anna");
		ages.add(12);

		names.add("Per");
		ages.add(3);

		names.add("Hans");
		ages.add(7);

		names.add("Lise");
		ages.add(9);
	}
	
	public static void printAll(ArrayList<String> names, ArrayList<Integer> ages) {
		for(int i=0; i<names.size(); i++) {
			System.out.println(names.get(i)+" is "+ ages.get(i)+" years.");
		}
	}
}
