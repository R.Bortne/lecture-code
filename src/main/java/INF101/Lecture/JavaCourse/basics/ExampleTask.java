package INF101.Lecture.JavaCourse.basics;

/**
 * Task: Create a new method that prints out the
 *  sentence "Jeg vil ha kake"
 */
public class ExampleTask {

    public static void main(String[] args) {
        printSentence();
    }

    public static void printSentence() {
        System.out.println("Jeg vil ha kake");
    }
    
}
