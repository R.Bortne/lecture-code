package INF101.Lecture.JavaCourse.util;

import java.util.ArrayList;

public class UtilityMethods {
    
    /**
     * Finds the largest integer in the list
     * @param list
     * @return largest integer
     */
    public static int max(ArrayList<Integer> list) {
        int largestElement = Integer.MIN_VALUE;
        for (int i = 0; i < list.size(); i++) {
            int elem = list.get(i);
            if (elem > largestElement) {
                largestElement = elem;
            }
        }
        return largestElement;
    }
}
